#include "Quad7Segment.h"

Quad7Segment qs(2,3,4,5);

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  int input = analogRead(A0);
  
  qs.setNumber(input);
  qs.update();
}
