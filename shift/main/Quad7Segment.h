#ifndef QUAD7SEGMENT_H
#define QUAD7SEGMENT_H
#include "HC595.h"

class Quad7Segment{
  public:
    static const unsigned long pattern[10];
    Quad7Segment(int ser, int srClk, int rClk, int srClr):
      shreg(ser,srClk,rClk,srClr){};
    void setup();
    void update();
    void setNumber(int n);
    unsigned long getNum(int n);
   //add other stuff if you need
  private:
    HC595 shreg;
    unsigned long numbers[4] = {0,0,0,0};

};

#endif
