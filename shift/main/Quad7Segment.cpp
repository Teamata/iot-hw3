#include "Quad7Segment.h"


//http://www.nutsvolts.com/magazine/article/using-seven-segment-displays-part-1

// 0 means on 1 means off for segminet
const unsigned long Quad7Segment::pattern[10] = {
  //edcgbfa(dp)
  0b111011101111, //0 
  0b001010001111, //1  
  0b110110101111, //2 
  0b011110101111, //3 1952
  0b001111001111, //4 
  0b011101101111, //5 
  0b111101101111, //6 
  0b001010101111, //7
  0b111111101111, //8
  0b011111101111 //9
};

// 0 means Off 1 means On for Digits
const unsigned long digits[4] = {
  0b111111111110, //0 
  0b111111111101, //1
  0b111111111011, //2
  0b111111110111, //3
};

void Quad7Segment::setup(){
}

void Quad7Segment::update(){
  //implement this
  for(int i = 0 ; i < 4; i++){
    shreg.pushn(numbers[i]&digits[i], 12);
    shreg.latch();
  }
}

unsigned long Quad7Segment::getNum(int n){
  return numbers[n]&digits[n];
}

void Quad7Segment::setNumber(int n){
  //implement this
  numbers[3] = pattern[n/1000];
  numbers[2] = pattern[(n/100) %10];
  numbers[1] = pattern[(n/10) %10];
  numbers[0] = pattern[(n) %10];
}
