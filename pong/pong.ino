/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
  Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/


//#include "ArduinoEmulator.h" //you won't need this for real arduino
#include "PongGame.h"
#include "LEDMatrix.h"

using namespace std;
using namespace hw4;

int test[8] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b10000001,
  0b11000001,
  0b10000001,
  0b00000000,
  0b00000000
};

int transform[8] = {
  0, 0, 0, 129, 193, 129, 0 , 0
};

int data[8] = {
  0b00000000,
  0b01000010,
  0b00000000,
  0b00011000,
  0b00011000,
  0b00000000,
  0b01000010,
  0b00111100
};

long previousMillis = 0;


const int nrow = 8;
const int ncol = 8;
LEDMatrix ledMat(2, 3, 4, 7);
const long interval = 100;           // interval at which to blink (milliseconds)
const int p1_l = 5;
const int p1_r = 6;
const int p2_l = 9;
const int p2_r = 10;
const int reset = 11;
const int buttons[5] = {reset, p1_l, p1_r, p2_l, p2_r};
const char mapping[5] = {'r', 'a' , 'b' , 'c' , 'd'};
int tmp[8] = {0, 0, 0, 0, 0, 0, 0}; 


PongGame pongGame(nrow, ncol);
int* buffer = new int[nrow * ncol];

/***********CHANGE THESE*********************/

void setup() {
  pongGame.start(millis());
  pinMode(p1_l, INPUT_PULLUP);
  pinMode(p1_r, INPUT_PULLUP);
  pinMode(p2_l, INPUT_PULLUP);
  pinMode(p2_r, INPUT_PULLUP);
  pinMode(reset, INPUT_PULLUP);
  Serial.begin(9600);
}


char getInput() {
  for (int i = 0; i < 5; i++) {
    int buttonState = digitalRead(buttons[i]);
    if (buttonState == LOW) {
      return mapping[i];
    }
  }
  return 'q';
}

void processInput(unsigned long tick) { //You will need to change this
  char ch = getInput();
//  Serial.println(ch);
  switch (ch) {
    case 'd':
      pongGame.movePad(Player::PLAYER_ONE, PadDirection::UP);
      break;
    case 'c':
      pongGame.movePad(Player::PLAYER_ONE, PadDirection::DOWN);
      break;
    case 'r':
      pongGame.reset();
      pongGame.start(tick);
      break;
    case 'b':
      pongGame.movePad(Player::PLAYER_TWO, PadDirection::UP);
      break;
    case 'a':
      pongGame.movePad(Player::PLAYER_TWO, PadDirection::DOWN);
      break;
    default:
      break;
  }

}

void padd(int* buff, int* tmp){
  for(int i = 0; i < 64 ; i += 8){
    int temporary = 0;
    for(int j = 0; j < 8 ; j ++) {
//      Serial.println(buff[i+j]);
      temporary += buff[i+j] << (7-j); 
//      delay(50);
//      Serial.println(buff[i+j] << (7-j));
//      tmp[i/8] += buff[i+j] << (7-j); 
    }
    tmp[i/8] = temporary;
//    Serial.println(temporary);
//    delay(50);
  }
}

void loop() {
      const unsigned long tick = millis();
      unsigned long currentMillis = millis();
      if(currentMillis - previousMillis > interval){
        previousMillis = currentMillis;  
  
        processInput(tick);
      }
      pongGame.update(tick);
//      if(pongGame.isDirty()){ //detects if needs to repaint work 
        pongGame.paint(buffer);
        padd(buffer, tmp);
        ledMat.writeData(tmp);
//        ledMat.writeData(padd(buffer));
        ledMat.update();
//      }
}
